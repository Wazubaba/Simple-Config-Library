/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#ifndef SICFG_H_YBVBPPLT
#define SICFG_H_YBVBPPLT

#include <stddef.h>
#include <stdint.h>

/*
	TODO:
		* support dot-path notation.
		* correctness, simplicity, and readability before speed:
				this will most likely not be run in performance-critical code
		* documentation.
		* comment type to preserve comments (needs next item first)
		* line number stored in node_t to preserve file structure (maybe)
*/

/*
	Internally, all decimal numbers are doubles and all normal numbers
	are an int64_t.

	This is to simplify the config format for the users, since the
	average user probably has no clue wtf the difference between a
	long and an int is, let alone a float vs a double...
*/

enum CFG_ERRORS
{
	CFG_NOERR,
	CFG_NOMEM,
	CFG_PARSEFAIL,
	CFG_KEYEXISTS,
	CFG_KEYNOTFOUND,
	CFG_INVALIDBUFFER
};

enum CFG_TYPES
{
	CFG_TYPEROOT,
	CFG_TYPEANY,
	CFG_TYPESTRING,
	CFG_TYPENUMBER,
	CFG_TYPEDECIMAL,
	CFG_TYPEBOOL,
	CFG_TYPEARRAY
};

enum CFG_PARSERRORS
{
	CFG_PARSENOTE,
	CFG_PARSEMESSAGE,
	CFG_PARSEWARNING,
	CFG_PARSEERROR
};

struct parse_message_t
{
	size_t line;
	uint8_t severity;
	char* info;
};

struct parse_results_t
{
	size_t length;
	struct parse_message_t** messages;
};

struct node_t
{
	uint8_t type;
	size_t length;
	char* key;
	char* val;
	struct node_t** contents;
};

struct parse_options_t
{
	uint8_t use_includes;
	uint8_t use_injections;
	uint8_t strict_injections;
};

/* I/O functions */

/*
	NOTE: just hand an empty, unallocated pointer to warnings. It will
	handle allocating space internally for the container.
*/
const uint8_t cfg_parse(struct node_t* cfg, struct parse_results_t* warnings, 
		struct parse_options_t* options, const char* include_path, const char* data);

/* Allocates space for buffer, so hand an empty char* */
const uint8_t cfg_serialize(const struct node_t* cfg, char* buffer);

void cfg_init(struct node_t* cfg);
inline struct node_t* cfg_create();
void array_init(struct node_t* array);
inline struct node_t* array_create();
void cfg_printleaf(const struct node_t* leaf, const uint8_t show_subelements);

void cfg_destroy(struct node_t* cfg);

/*
	If buffer is NULL, will not try to return a pointer, otherwise it will
	contain a pointer to the node_t if found. If not found but not NULL will
	be set to NULL by the function.
*/
const uint8_t cfg_findkey(const struct node_t* cfg, struct node_t* buffer, const char* key, const uint8_t type, size_t depth);

const uint8_t cfg_getstring(const struct node_t* cfg, const char* key, char* buffer);
const uint8_t cfg_getnumber(const struct node_t* cfg, const char* key, int64_t* buffer);
const uint8_t cfg_getdecimal(const struct node_t* cfg, const char* key, double* buffer);
const uint8_t cfg_getbool(const struct node_t* cfg, const char* key, uint8_t* buffer);
const uint8_t cfg_getarray(const struct node_t* cfg, const char* key, struct node_t* buffer);

/* Set an existing value to an array or config */
const uint8_t cfg_setstring(struct node_t* cfg, const char* key, const char* value);
const uint8_t cfg_setnumber(struct node_t* cfg, const char* key, const int64_t value);
const uint8_t cfg_setdecimal(struct node_t* cfg, const char* key, const double value);
const uint8_t cfg_setbool(struct node_t* cfg, const char* key, const uint8_t value);

/*
	Warning: This will delete the old array, so take care to not do anything with
	pointers to it at this point.

	See the warning about cfg_pusharray() as well for further information.
*/
const uint8_t cfg_setarray(struct node_t* cfg, const char* key, struct node_t* value);

/* Append a new value to an array or config */
const uint8_t cfg_pushstring(struct node_t* cfg, const char* key, const char* value);
const uint8_t cfg_pushnumber(struct node_t* cfg, const char* key, const int64_t value);
const uint8_t cfg_pushdecimal(struct node_t* cfg, const char* key, const double value);
const uint8_t cfg_pushbool(struct node_t* cfg, const char* key, const uint8_t value);

/*
	Warning: This will will make the array you pass to it owned by the config in
	order not to have to duplicate a large amount of data.

	This basically means that you should not manually free this array yourself;
	instead it will be deleted when you call cfg_destroy() on the config itself.
*/
const uint8_t cfg_pusharray(struct node_t* cfg, const char* key, struct node_t* value);


const uint8_t cfg_deletekey(struct node_t* cfg, const char* key, size_t depth);


/* Stuff for handling parse results */
struct parse_results_t* cfg_create_parseresults();
void cfg_destroy_parseresults(struct parse_results_t* results);

#endif /* end of include guard: SICFG_H_YBVBPPLT */
