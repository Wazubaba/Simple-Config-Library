# Configuration file format #

## Preprocessor Commands ##

Command                        | Description
-------------------------------|------------
`include` *<path/to/file.cfg>* | Include a target file that is relative to the include path specified when `cfg_parse` is called.
`message` *message contents*   | Emit a `CFG_PARSEMESSAGE` to the parsewarnings array.


## Injections ##
Injections are basic string substitution devices. It allows you to
	add the contents of another key into the current string.

You specify the target key to inject into the string via a simple
	dot-path notation. There are three ways to do this:

1. `${path.to.key}` - Standard method. Will first test local to the
	array or root that contains this string, and failing that will try
	to expand to it from the root config.

2. `${.path.to.key}` - Root method. Will only test from the global
	root of the config.

3. `${-path.to.key}` - Local method. Will only test local to the
	current string, ignoring the global scope.

The `cfg_parse` function's `strict_injections` variable controls
	whether to let a failed injection be simply ignored and treated
	as part of the value, or to abort parsing there.


> ##### Example ######
> Given:
> ```ini
> key = "${missing name} fell into the well"
> ```
> key would resolve to precisely `"${missing name} fell into the well` should
> there be no key found and `strict_injections` is disable. It will still be
> included in the parse_errors array.
