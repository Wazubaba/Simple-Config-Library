/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#include "sicfg.h"
#include "parser.h"
#include "generation.h"

#include <string.h>
#include <ctype.h>
#include <malloc.h>

const uint8_t cfg_parse(struct node_t* cfg, struct parse_results_t* warnings, 
		struct parse_options_t* options, const char* include_path, const char* data)
{
	uint8_t result = CFG_NOERR;

	if (data == NULL || strlen(data) == 0)
		return CFG_INVALIDBUFFER;


	char* _buffer = NULL;
	if (options->use_includes || options->use_injections)
	{
		_buffer = strdup(data);
		if (parser_include_phase(warnings, _buffer, include_path) == CFG_NOMEM ||
			parser_injection_phase(warnings, _buffer, options->strict_injections) == CFG_NOMEM)
		{
			result = CFG_NOMEM;
			goto failure;	
		}
	}
	else
		_buffer = (char*) data;

	if (decipher_data(cfg, warnings, options, _buffer) == CFG_NOMEM)
		result = CFG_NOMEM;

failure:

	if (options->use_includes || options->use_injections)
		free(_buffer);

	if (result != CFG_NOERR)
	{
		cfg_destroy_parseresults(warnings);
		cfg_destroy(cfg);
	}
	return result;
}
