/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#ifndef PARSER_H_FOCAPTYM
#define PARSER_H_FOCAPTYM

#include "sicfg.h"
#include <stdint.h>

/*
	Parse a given line of data. This is meant to be used with normal data;
	arrays are handled in the main driver but use this for parsing their
	sub-values.
*/
const uint8_t decipher_data(struct node_t* cfg, struct parse_results_t* warnings, 
		struct parse_options_t* options, const char* data);

#endif /* end of include guard: PARSER_H_FOCAPTYM */
