/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#include "sicfg.h"
#include "warnings.h"

#include <string.h>
#include <stddef.h>
#include <malloc.h>

const uint8_t push_message(struct parse_results_t* results, const uint8_t severity, const size_t lineno, const char* info)
{
	struct parse_message_t* message = calloc(1, sizeof(struct parse_message_t));
	if (!message)
		return CFG_NOMEM;
	
	message->line = lineno;
	message->severity = severity;
	message->info = strdup(info);

	struct parse_message_t** temp = realloc(results->messages, sizeof(struct parse_message_t*) * (results->length + 1));
	if (!temp)
	{
		free(message->info);
		free(message);
		return CFG_NOMEM;
	}

	results->messages = temp;
	results->messages[results->length++] = message;

	return CFG_NOERR;
}

struct parse_results_t* cfg_create_parseresults()
{
	struct parse_results_t* results = calloc(1, sizeof(struct parse_results_t));
	if (!results)
		return NULL;
	results->length = 0;
	results->messages = NULL;

	return results;
}

void cfg_destroy_parseresults(struct parse_results_t* results)
{
	if (results->messages)
	{
		size_t itr;
		for (itr = 0; itr < results->length; itr++)
		{
			if (results->messages[itr])
			{
				if (results->messages[itr]->info)
					free(results->messages[itr]->info);
				
				free(results->messages[itr]);
			}
		}
		
		free(results->messages);
	}
	if (results)
		free(results);
}
