/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#include "sicfg.h"
#include "warnings.h"
#include "generation.h"

#include <string.h>
#include <ctype.h>
#include <malloc.h>
#include <stdlib.h>

size_t substr(const char* str, char* buffer, const size_t begin, const size_t end)
{
	size_t subidx = 0;
	size_t itr;
	int64_t remaining = strlen(str);

	for (itr = begin; itr < end; itr++)
	{
		if (--remaining < 0) break;
		buffer[subidx++] = str[itr];
	}
	
	return end - itr + 1;
}

uint8_t islinebreak(const char* str)
{
	char c;
	size_t itr = 0;
	while ((c = str[itr++]) != '\0')
		if (!isspace(c))
			return 0;
	
	return 1;
}

/*
	Shift all chars to the left to fill blanks
*/
uint8_t cleanup(char* src)
{
	if (!src || strlen(src) == 0) return CFG_INVALIDBUFFER;
	
	char* dest = calloc(strlen(src), sizeof(char));
	if (!dest)
		return CFG_NOMEM;
	
	int64_t src_itr = 0;
	size_t dest_itr = 0;
	
	size_t front = 0;
	size_t back = 0;

	uint8_t flag = 0;

	for (src_itr = 0; src_itr < strlen(src); src_itr++)
	{
		if (isspace(src[src_itr]))
			front++;
		else
			break;
	}

	for (src_itr = strlen(src) - 1; src_itr >= 0; src_itr--)
	{
		if (isspace(src[src_itr]))
			back++;
		else
			break;
	}

	for (src_itr = front; src_itr < strlen(src) - back; src_itr++)
	{
		dest[dest_itr++] = src[src_itr];
	}
	
	dest[dest_itr] = '\0';
	strcpy(src, dest);
	free(dest);
	return CFG_NOERR;
}

/*
	Order of variable type determination:
		First a value is tested to see if it is a decimal.
		After that a number.
		barring that, we test if it is a bool.
		finally, failing all options, we assume it is a string.
	
	idea: force string values to have a double-quote delimeter.
	This way if we will always know if something is a number or a
	string. From there we can simply test if there is a decimal marker
	in them to determine if it is a decimal type or a simple number type.
*/

/*
	The way concat works is that within the current cfg ref, if an
	include is found, it will concat that entire file as if it were
	intended to be part of the array. The alternative form of this
	is to assign an include to a variable:
		myvar: @include samplefile.cfg
	would make myvar an array containing the contents of samplefile.cfg.
*/

/*
	We might be able to make semicolons optional. Number values would
	never contain a newline within them, only strings - and strings
	will *always* be surrounded by enclosing double-quotes. Due to this,
	the only structure that will ever be at risk of spanning multiple
	lines are strings, who are already marked as ending by the finishing
	double-quote.
*/

/*
	To support includes, we will have to add file i/o to the library.
	This will require three phases:
		1) stitch in includes
		2) stitch in injected values
		3) finally parse the whole thing
	
	Includes should be optional. This will lead to a bit of a headache
	later when injections refer to values found only in includes.

	We also need to support the dot-path-notation schema for finding
	key values within the configs for the injector to work.

	General rules:
		if a path is provided as dot-notation it should first be tested
		from the local root (this means a containing array, if applicable).
		
		If it cannot be found, it will fallback to testing within the root
		of the config.

		This behaviour can be enforced by specifying the include method
		via the proper preprocessor commands.
*/

/*
	if an array is found, hand a new node_t* container to decipher_data
	in the recursive call to popluate with all subsequent data, and then
	finally just cfg_appendarray() it onto the cfg.
*/
/*
	This function will recursively call itself if it finds an array.
*/

#include <assert.h>
const uint8_t decipher_data(struct node_t* cfg, struct parse_results_t* warnings, 
		struct parse_options_t* options, const char* data)
{
	size_t datalen = strlen(data);

	size_t data_itr = 0;
	size_t buffer_itr = 0;
	size_t array_begin = 0;
	size_t array_end = 0;

	size_t lineno = 1;

	/* Flags used to manage state */
	uint8_t in_string = 0;
	uint8_t in_comment = 0;
	uint8_t in_value = 0;
	uint8_t in_array = 0;

	/*
		At absolute worse, buffer might have to hold almost as much data
		as the file contains
	*/
	char* buffer = calloc(strlen(data), sizeof(char));
	if (!buffer)
		return CFG_NOMEM;
	
	char val[4096] = {0};
	char key[4096] = {0};

	for (data_itr = 0; data_itr < datalen; data_itr++)
	{
		char sym = data[data_itr];

		switch (sym)
		{
			case '\n':
				buffer[buffer_itr + 1] = '\0';
				if (!in_value && !in_comment && !in_string && !in_array && islinebreak(buffer))
				{
					push_message(warnings, CFG_PARSEERROR, lineno, "Newline found before value");
					goto cleanup;
				}

				if (strncmp(buffer, "true", 5))
					cfg_pushbool(cfg, key, 1);
				if (strncmp(buffer, "false", 6))
					cfg_pushbool(cfg, key, 0);
				
				if (isdigit(buffer[0]))
				{
					if (strchr(buffer, '.'))
						cfg_pushdecimal(cfg, key, strtod(buffer, NULL));
					else
						cfg_pushnumber(cfg, key, strtoll(buffer, NULL, 10));
				}
				else
				{
					/* Assume it is a string value */
					cfg_pushstring(cfg, key, buffer);
				}




				buffer_itr = 0;
				array_begin = 0;
				array_end = 0;
				in_comment = 0;
				in_value = 0;
				lineno++;
				break;
			
			case '#':
				in_comment = 1;
				break;
			
			case '"':
				in_string = !in_string;
				break;
			
			case '=':
			case ':':
				if (!in_comment)
				{
					buffer[buffer_itr] = '\0';
					if (!cleanup(buffer) == CFG_NOERR)
					{
						fprintf(stderr, "Unhandled issue on %s[%d]\n", __FILE__, __LINE__);
					}
					strcpy(key, buffer);
					buffer_itr = 0;

					in_value = 1;
				}
			default:
				buffer[buffer_itr++] = sym;
		}	
	}
cleanup:
	free(buffer);
	return CFG_NOERR;
}
