/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#include "generation.h"
#include "warnings.h"

#include <malloc.h>
#include <inttypes.h>
#include <stddef.h>

/* Stage 1: Process includes */
const uint8_t parser_include_phase(struct parse_results_t* messages, char* buffer, const char* include_root)
{
	if (push_message(messages, CFG_PARSENOTE, 0, "INCLUDE PHASE NOT YET IMPLEMENTED") == CFG_NOMEM)
		return CFG_NOMEM;

	return CFG_NOERR;
}

/* Stage 2: Process string injections */
const uint8_t parser_injection_phase(struct parse_results_t* messages, char* buffer, const uint8_t strict_injections)
{
	if (push_message(messages, CFG_PARSENOTE, 0, "INJECTION PHASE NOT YET IMPLEMENTED") == CFG_NOMEM)
		return CFG_NOMEM;

	return CFG_NOERR;
}
