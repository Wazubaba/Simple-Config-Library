/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#ifndef WARNINGS_H_MIW92VDJ
#define WARNINGS_H_MIW92VDJ

#include <stddef.h>
#include <stdint.h>

/*inline const uint8_t cfg_parseresult_append(struct parse_results_t* results,
		struct parse_message_t* message);*/

inline const uint8_t push_message(struct parse_results_t* results, const uint8_t severity, const size_t lineno, const char* info);

/* Stuff for individual parse messages */
/*inline void cfg_parsemessage_init(struct parse_message_t* message, const uint8_t severity, const size_t lineno, const char* info);*/
inline void cfg_parsemessage_destroy(struct parse_message_t* message);
#endif /* end of include guard: WARNINGS_H_MIW92VDJ */
