/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#include "sicfg.h"

#include <malloc.h>
#include <stddef.h>
#include <string.h>

struct node_t* cfg_create()
{
	struct node_t* result = calloc(1, sizeof(struct node_t));
	if (!result)
		return NULL;
	
	result->type = CFG_TYPEROOT;
	result->key = NULL;
	result->val = NULL;
	result->length = 0;
	result->contents = NULL;

	return result;
}

struct node_t* array_create()
{
	struct node_t* result = cfg_create();
	if (!result)
		return NULL;
	
	result->type = CFG_TYPEARRAY;

	return result;
}

void cfg_init(struct node_t* cfg)
{
	cfg->type = CFG_TYPEROOT;
	cfg->key = NULL;
	cfg->val = NULL;
	cfg->length = 0;
	cfg->contents = NULL;
}

void array_init(struct node_t* array)
{
	array->type = CFG_TYPEARRAY;
	array->key = NULL;
	array->val = NULL;
	array->length = 0;
	array->contents = NULL;
}

/*
	Recursively iterate contents untill everything is freed.
	After that just clean up the stuff that needs cleaning and finally
	free self unless root node, which is up to the user to clean up
*/
void cfg_destroy(struct node_t* cfg)
{
	if (cfg->contents != NULL && cfg->length > 0)
	{
		size_t itr;
		for (itr = 0; itr < cfg->length; itr++)
			cfg_destroy(cfg->contents[itr]);
		
		free(cfg->contents);
	}
	if (cfg->val != NULL)
		free(cfg->val);
	if (cfg->key != NULL)
		free(cfg->key);
	free(cfg);
}

const uint8_t _findkey_recursion(const struct node_t* cfg, struct node_t* buffer, const char* key, const uint8_t type, size_t depth)
{
	size_t itr;
	for (itr = 0; itr < cfg->length; itr++)
	{
		struct node_t* ref = cfg->contents[itr];
		if (ref->type == CFG_TYPEARRAY)
		{
			if (depth != 0)
			{
				if (cfg_findkey(ref, buffer, key, type, --depth))
					return 1;
			}
		}

		if (ref->type != type && type != CFG_TYPEANY) continue;

		if (strcmp(cfg->contents[itr]->key, key) == 0)
		{
			if (buffer)
				buffer = ref;
			return 1;
		}
	}

	return 0;
}

const uint8_t cfg_findkey(const struct node_t* cfg, struct node_t* buffer, const char* key, const uint8_t type, size_t depth)
{
	if (_findkey_recursion(cfg, buffer, key, type, depth))
		return 1;
	
	if (buffer)
		buffer = NULL;

	return 0;
}

const uint8_t cfg_deletekey(struct node_t* cfg, const char* key, size_t depth)
{
	struct node_t* ref;
	if (!cfg_findkey(cfg, ref, key, CFG_TYPEANY, 0))
		return CFG_KEYNOTFOUND;

	cfg_destroy(ref);
	return CFG_NOERR;
}
