/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#include "sicfg.h"

#include <malloc.h>
#include <string.h>
#include <inttypes.h>

const uint8_t cfg_setstring(struct node_t* cfg, const char* key, const char* value)
{
	struct node_t* ref;
	if (!cfg_findkey(cfg, ref, key, CFG_TYPESTRING, 0))
		return CFG_KEYNOTFOUND;
	
	free(ref->val);
	ref->val = strdup(value);

	return CFG_NOERR;
}

const uint8_t cfg_setnumber(struct node_t* cfg, const char* key, const int64_t value)
{
	struct node_t* ref;
	if (!cfg_findkey(cfg, ref, key, CFG_TYPENUMBER, 0))
		return CFG_KEYNOTFOUND;

	char* buffer = calloc(128, sizeof(char));
	if (!buffer)
		return CFG_NOMEM;

	free(ref->val);

	snprintf(buffer, 128, "%"PRIi64, value);
	ref->val = buffer;

	return CFG_NOERR;
}

const uint8_t cfg_setdecimal(struct node_t* cfg, const char* key, const double value)
{
	struct node_t* ref;
	if (!cfg_findkey(cfg, ref, key, CFG_TYPEDECIMAL, 0))
		return CFG_KEYNOTFOUND;

	char* buffer = calloc(128, sizeof(char));
	if (!buffer)
		return CFG_NOMEM;
	
	free(ref->val);

	snprintf(buffer, 128, "%e", value);
	ref->val = buffer;

	return CFG_NOERR;
}

const uint8_t cfg_setbool(struct node_t* cfg, const char* key, const uint8_t value)
{
	struct node_t* ref;
	if (!cfg_findkey(cfg, ref, key, CFG_TYPEBOOL, 0))
		return CFG_KEYNOTFOUND;

	char* buffer = calloc(8, sizeof(char));
	if (!buffer)
		return CFG_NOMEM;

	free(ref->val);

	if (value != 0)
		strcpy(buffer, "true");
	else
		strcpy(buffer, "false");
	
	ref->val = buffer;

	return CFG_NOERR;
}

/*
	Warning: This will delete the old array, so take care to not do anything with
	pointers to it at this point.

	See the warning about cfg_pusharray() as well for further information.
*/
const uint8_t cfg_setarray(struct node_t* cfg, const char* key, struct node_t* value)
{
	struct node_t* ref;
	if (!cfg_findkey(cfg, ref, key, CFG_TYPEARRAY, 0))
		return CFG_KEYNOTFOUND;
	
	cfg_destroy(ref);
	ref = value;

	return CFG_NOERR;
}
