/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#include "sicfg.h"

#include <stdio.h>
#include <stddef.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>


struct node_t* _recursion_helper(const struct node_t* array, const char* key, const uint8_t type)
{
	size_t itr;
	for (itr = 0; itr < array->length; itr++)
	{
		struct node_t* ref = array->contents[itr];
		if (ref->type == type)
		{
			if (strcmp(ref->key, key) == 0)
				return ref;
		}
		else
		if (ref->type == CFG_TYPEARRAY)
		{
			struct node_t* attempt = _recursion_helper(ref, key, type);
			if (attempt != NULL)
				return attempt;
		}
	}

	return NULL;
}

const uint8_t convert_number(const char* value, int64_t* buffer)
{
	if (sscanf(value, "%"SCNd64, buffer))
		return 1;
	else
		return 0;
}



const uint8_t cfg_getstring(const struct node_t* cfg, const char* key, char* buffer)
{
	const struct node_t* attempt = _recursion_helper(cfg, key, CFG_TYPESTRING);
	if (attempt != NULL)
	{
		buffer = attempt->val;
		return 1;
	}

	return 0;
}

const uint8_t cfg_getnumber(const struct node_t* cfg, const char* key, int64_t* buffer)
{
	const struct node_t* attempt = _recursion_helper(cfg, key, CFG_TYPENUMBER);
	if (attempt != NULL)
	{
		convert_number(attempt->val, buffer);
		return 1;
	}
	return 0;
}

const uint8_t cfg_getdecimal(const struct node_t* cfg, const char* key, double* buffer)
{
	const struct node_t* attempt = _recursion_helper(cfg, key, CFG_TYPEDECIMAL);
	if (attempt != NULL)
	{
		*buffer = atof(attempt->val);
		return 1;
	}
	return 0;
}

const uint8_t cfg_getbool(const struct node_t* cfg, const char* key, uint8_t* buffer)
{
	const struct node_t* attempt = _recursion_helper(cfg, key, CFG_TYPEBOOL);
	if (attempt != NULL)
	{
		*buffer = (uint8_t) atoi(attempt->val);
		return 1;
	}
	return 0;
}

/* Recursively walk all of the things ever. */
const uint8_t cfg_getarray(const struct node_t* cfg, const char* key, struct node_t* buffer)
{
	const struct node_t* attempt = _recursion_helper(cfg, key, CFG_TYPEARRAY);
	if (attempt != NULL)
	{
		buffer = (struct node_t*) attempt;
		return 1;
	}
	return 0;
}
