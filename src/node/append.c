/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#include "sicfg.h"

#include <inttypes.h>
#include <string.h>
#include <malloc.h>

#include <stdio.h>

uint8_t expand(struct node_t* cfg)
{
	struct node_t* prefab = calloc(1, sizeof(struct node_t));
	if (!prefab)
		return CFG_NOMEM;

	struct node_t** temp;
	if (!cfg->contents)
		temp = (struct node_t**) calloc(1, sizeof(struct node_t*));

	else
		temp = (struct node_t**) realloc(cfg->contents, sizeof(struct node_t*) * (cfg->length + 1));

	if (!temp)
	{
		free(prefab);
		return CFG_NOMEM;
	}

	else
	cfg->contents = temp;
	cfg->contents[cfg->length] = prefab;
	cfg->length++;
	return CFG_NOERR;
}

const uint8_t cfg_pushstring(struct node_t* cfg, const char* key, const char* value)
{
	if (cfg_findkey(cfg, NULL, key, CFG_TYPEANY, 0))
		return CFG_KEYEXISTS;

	if (expand(cfg) == CFG_NOMEM)
		return CFG_NOMEM;
	
	struct node_t* new_entry = cfg->contents[cfg->length - 1];
	new_entry->type = CFG_TYPESTRING;
	new_entry->key = strdup(key);
	new_entry->val = strdup(value);
	new_entry->length = 0;
	new_entry->contents = NULL;

	return CFG_NOERR;
}

const uint8_t cfg_pushnumber(struct node_t* cfg, const char* key, const int64_t value)
{
	if (cfg_findkey(cfg, NULL, key, CFG_TYPEANY, 0))
		return CFG_KEYEXISTS;

	if (expand(cfg) == CFG_NOMEM)
		return CFG_NOMEM;
		
	char buffer[256];

	snprintf(buffer, 128, "%"PRIi64, value);

	struct node_t* new_entry = cfg->contents[cfg->length - 1];
	new_entry->type = CFG_TYPENUMBER;
	new_entry->key = strdup(key);
	new_entry->val = strdup(buffer);
	new_entry->length = 0;
	new_entry->contents = NULL;

	return CFG_NOERR;
}

const uint8_t cfg_pushdecimal(struct node_t* cfg, const char* key, const double value)
{
	if (cfg_findkey(cfg, NULL, key, CFG_TYPEANY, 0))
		return CFG_KEYEXISTS;

	char* buffer = calloc(128, sizeof(char));
	if (!buffer)
		return CFG_NOMEM;

	if (expand(cfg) == CFG_NOMEM)
	{
		free(buffer);
		return CFG_NOMEM;
	}

	snprintf(buffer, 128, "%e", value);

	struct node_t* new_entry = cfg->contents[cfg->length - 1];
	new_entry->type = CFG_TYPEDECIMAL;
	new_entry->key = strdup(key);
	new_entry->val = buffer;
	new_entry->length = 0;
	new_entry->contents = NULL;

	return CFG_NOERR;
}

const uint8_t cfg_pushbool(struct node_t* cfg, const char* key, const uint8_t value)
{
	if (cfg_findkey(cfg, NULL, key, CFG_TYPEANY, 0))
		return CFG_KEYEXISTS;

	char* buffer = calloc(8, sizeof(char));
	if (!buffer)
		return CFG_NOMEM;

	if (expand(cfg) == CFG_NOMEM)
	{
		free(buffer);
		return CFG_NOMEM;
	}

	if (value != 0)
		strcpy(buffer, "true");
	else
		strcpy(buffer, "false");

	struct node_t* new_entry = cfg->contents[cfg->length - 1];
	new_entry->type = CFG_TYPEBOOL;
	new_entry->key = strdup(key);
	new_entry->val = buffer;
	new_entry->length = 0;
	new_entry->contents = NULL;

	return CFG_NOERR;
}

const uint8_t cfg_pusharray(struct node_t* cfg, const char* key, struct node_t* value)
{
	if (cfg_findkey(cfg, NULL, key, CFG_TYPEANY, 0))
		return CFG_KEYEXISTS;

	if (expand(cfg) == CFG_NOMEM)
		return CFG_NOMEM;

	free(cfg->contents[cfg->length - 1]);
	cfg->contents[cfg->length - 1] = value;

	return CFG_NOERR;
}
