/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#include "sicfg.h"
#include <stddef.h>
#include <stdio.h>

void _printleaf_indent(const struct node_t* leaf, const int indent)
{
	char prefix [512];
	size_t itr;
	for (itr = 0; itr < indent; itr++)
		prefix[itr] = ' ';
	prefix[indent] = '\0';

	printf("%s[------\n", prefix);
	printf("%stype: ", prefix);
	switch (leaf->type)
	{
		case CFG_TYPEARRAY:
			puts("array");
			break;
		case CFG_TYPENUMBER:
			puts("number");
			break;
		case CFG_TYPESTRING:
			puts("string");
			break;
		case CFG_TYPEBOOL:
			puts("bool");
			break;
		case CFG_TYPEDECIMAL:
			puts("decimal");
			break;
		case CFG_TYPEROOT:
			puts("root");
			break;
	}
	printf("%skey: %s\n", prefix, leaf->key);
	printf("%sval: %s\n", prefix, leaf->val);

	printf("%snumber of sub elements: %zu\n", prefix, leaf->length);

	for (itr = 0; itr < leaf->length; itr++)
		_printleaf_indent(leaf->contents[itr], indent + 1);

	printf("%s------]\n", prefix);
}

void cfg_printleaf(const struct node_t* leaf, const uint8_t show_subelements)
{
	puts("[------");
	printf("type: ");
	switch (leaf->type)
	{
		case CFG_TYPEARRAY:
			puts("array");
			break;
		case CFG_TYPENUMBER:
			puts("number");
			break;
		case CFG_TYPESTRING:
			puts("string");
			break;
		case CFG_TYPEBOOL:
			puts("bool");
			break;
		case CFG_TYPEDECIMAL:
			puts("decimal");
			break;
		case CFG_TYPEROOT:
			puts("root");
			break;
	}
	printf("key: %s\n", leaf->key);
	printf("val: %s\n", leaf->val);

	printf("number of sub elements: %zu\n", leaf->length);
	if (show_subelements)
	{
		size_t itr;
		for (itr = 0; itr < leaf->length; itr++)
			_printleaf_indent(leaf->contents[itr], 1);
	}
	else
		puts("---ommitting sub elements by request---");
	puts("------]");
}
