#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include <stdint.h>
#include "sicfg.h"

int main(void)
{
	struct node_t config;
	cfg_init(&config);

	cfg_pushstring(&config, "string test", "rawrmod");

	if (cfg_pushnumber(&config, "number test", 128) == CFG_NOMEM)
	{
		fprintf(stderr, "Failed to allocate memory!");
		return -1;
	}

	if (cfg_pushbool(&config, "bool test", 1) == CFG_NOMEM)
		fprintf(stderr, "Failed to allocate memory");
	cfg_pushdecimal(&config, "test", 0.5);
	
	struct node_t* array = calloc(1, sizeof(struct node_t));

	array_init(array);

	cfg_pushstring(array, "recursion string test", "wheeee!");

	cfg_pusharray(&config, "array test", array);

	int64_t number;

	/*
		Due to ctest this will only ever draw if you manually
		run the test binary :D
	*/
	cfg_printleaf(&config, 1);

	assert (cfg_getnumber(&config, "number test", &number));

	cfg_destroy(&config);
	return 0;
}
