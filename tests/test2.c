/*
	This file is a part of libsicfg.

	libsicfg is free software : you can redistribute it and / or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	libsicfg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with libsicfg.If not, see < http: //www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include <stdint.h>

#include "sicfg.h"

int main(void)
{
	struct node_t* config = cfg_create();
	struct parse_results_t* results = cfg_create_parseresults();
	struct parse_options_t options = {1, 1, 1};

	FILE* fp = fopen("tests/mockup.cfg", "r");
	if (!fp)
	{
		cfg_destroy_parseresults(results);
		cfg_destroy(config);
		fprintf(stderr, "failed to open config file\n");
		return 1;
	}
	/* Get size of the buffer */
	fseek(fp, 0L, SEEK_END);
	size_t size = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	/* +1 so we have room for \0 */
	char* buffer = calloc(size + 1, sizeof(char));
	if (!buffer)
	{
		cfg_destroy_parseresults(results);
		cfg_destroy(config);
		fclose(fp);
		fprintf(stderr, "failed to allocate memory for file buffer\n");
		return 1;
	}
	fread(buffer, sizeof(char), size, fp);
	fclose(fp);

	if (cfg_parse(config, results, &options, ".", buffer) == CFG_NOMEM)
	{
		cfg_destroy_parseresults(results);
		cfg_destroy(config);
		free(buffer);
		fprintf(stderr, "cfg_parse failed to allocate memory\n");
		return 1;
	}

	free(buffer);

	if (!results)
	{
		fprintf(stderr, "results array has been lost somehow o_o\n");
		return 1;
	}

	

	size_t itr;
	for (itr = 0; itr < results->length; itr++)
	{
		struct parse_message_t* message = results->messages[itr];
		switch(message->severity)
		{
			case CFG_PARSEERROR:
				fprintf(stderr, "[ERROR]");
				break;
			case CFG_PARSEFAIL:
				fprintf(stderr, "[FAIL]");
				break;
			case CFG_PARSEMESSAGE:
				fprintf(stderr, "[MESSAGE]");
			case CFG_PARSENOTE:
				fprintf(stderr, "[NOTE]");
		}

		fprintf(stderr, "<%zu> %s\n", message->line, message->info);
	}

	for (itr = 0; itr < config->length; itr++)
	{
		printf("Found entry: [%s] = %s\n", config->contents[itr]->key, config->contents[itr]->val);
	}

	cfg_destroy_parseresults(results);
	cfg_destroy(config);
	return 0;
}
